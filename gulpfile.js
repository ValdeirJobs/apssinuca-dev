var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    rename = require('gulp-rename');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var uglifycss = require('gulp-uglifycss');
var minifycss = require('gulp-minify-css');
var sass = require('gulp-sass');
var gulpSequence = require('gulp-sequence');
var browserSync = require('browser-sync');

gulp.task('browser-sync', function() {
  var files = [
    './styles.css',
    './*php',
    './js/*.js',
    './css/all/*.css'
  ];

  browserSync.init(files,{    
    proxy: "http://localhost:8000/",
    notify: false
  });
  
});

gulp.task('bs-reload', function () {
  browserSync.reload();
});


gulp.task('styles', function(){
  gulp.src(['dev/sass/**/*.scss'])
    .pipe(plumber({
      errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
    }}))
    .pipe(sass())
    .pipe(autoprefixer())
    .pipe(gulp.dest('wp-content/themes/aps/assets/css/'))
    .pipe(rename({suffix: '.min'}))
    .pipe(minifycss())
    .pipe(gulp.dest('wp-content/themes/aps/assets/css/'))    
    .pipe(browserSync.reload({stream:true}))
});

gulp.task('concatcss', function(){
  return gulp.src('wp-content/themes/aps/assets/css/*.css')
    .pipe(plumber({
      errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
    }}))
    .pipe(uglifycss())
    .pipe(concat('all.css'))
    .pipe(gulp.dest('wp-content/themes/aps/assets/css/all'))
    .pipe(browserSync.reload({stream:true}))
});

gulp.task('scripts', function(){
  return gulp.src('dev/js/**/*.js')
    .pipe(plumber({
      errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
    }}))
    .pipe(concat('main.js'))
    .pipe(gulp.dest('wp-content/themes/aps/assets/js/'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('wp-content/themes/aps/assets/js/'))
    .pipe(browserSync.reload({stream:true}))
});

gulp.task('watch', function(){
  gulp.watch("dev/sass/**/*.scss", ['styles']);
  gulp.watch("wp-content/themes/aps/assets/**/*.css", ['styles'] );
  gulp.watch("wp-content/themes/aps/assets/**/*.css", ['concatcss']);
  gulp.watch("dev/js/**/*.js", ['scripts']);
  gulp.watch("*.html", ['bs-reload']);
});

gulp.task('default', gulpSequence('styles', 'concatcss' ,'scripts', 'browser-sync', 'watch', 'bs-reload'))